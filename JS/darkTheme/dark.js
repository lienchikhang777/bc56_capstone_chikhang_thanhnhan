function changeTheme() {
    var body = document.querySelector('body');
    var nav = document.getElementById("nav-js");
    var navshow = document.getElementById("navbarSupportedContent");
    var btn = document.getElementById("dark-js");
    var iconBtn = btn.querySelector('i');
    var studios = document.getElementById("studios-js");
    var introduce = document.getElementById("introduce-js");
    var number = document.getElementById("number-js");
    var feedBack = document.getElementById("feedBack-js");
    var instructor = document.getElementById("instructor-js");
    var contact = document.getElementById("contact-js");
    var footer = document.getElementById("footer-js");
    var backToTop = document.getElementById("backToTop-js");

    // btn.innerHTML = `
    //     <i class="fa fa-sun"></i>
    // `
    console.log(iconBtn)
    body.classList.toggle('dark-js');
    nav.classList.toggle('dark-js');
    navshow.classList.toggle('dark-js');
    iconBtn.classList.toggle('fa-sun');
    if (iconBtn.classList.contains('fa-sun')) {
        iconBtn.classList.remove('fa-moon');
    }
    else {
        iconBtn.classList.add('fa-moon');
    }
    studios.classList.toggle('dark-js');
    introduce.classList.toggle('dark-js');
    number.classList.toggle('dark-js');
    feedBack.classList.toggle('dark-js');
    instructor.classList.toggle('dark-js');
    contact.classList.toggle('dark-js');
    footer.classList.toggle('dark-js');
    backToTop.classList.toggle('dark-js');
}