const fheader = document.querySelector("#site-header");
const btnshow = document.querySelector("#button_show");
const backgroundNavMenu = document.querySelector("#nav-js");

var clickCount = 0;
document
  .querySelector(".navbar-toggler")
  .addEventListener("click", function () {
    // ngăn người dùng cuộn chuột hay thao tác khác trước khi thoát khỏi phần show
    document.body.style.overflow = "hidden";

    if (clickCount === 0) {
      btnshow.classList.add("fa-xmark");
      btnshow.classList.remove("fa-bars");

      clickCount = 1;
    } else if (clickCount === 1) {
      fheader.style.height = "";
      document.body.style.overflow = "";
      fheader.style.border = "";
      btnshow.classList.remove("fa-xmark");
      btnshow.classList.add("fa-bars");

      clickCount = 0;
    }
  });

window.addEventListener("scroll", function () {
  // Kiểm tra xem trang đã được cuộn xuống hay chưa
  if (window.scrollY > 0) {
    fheader.classList.add("scrolled");
    backgroundNavMenu.classList.add("bgNav");
    backgroundNavMenu.classList.remove("bgw");
  } else {
    fheader.classList.remove("scrolled");
    backgroundNavMenu.classList.remove("bgNav");
    backgroundNavMenu.classList.add("bgw");
  }
});
